var vm = new Vue({
    el: '#app',
    data: {
        moves: [],
        healthRegen: {
            min: 5,
            max: 10
        },
        normalAttack: {
            minDamage: 5,
            maxDamage: 10 
        },
        specialAttack: {
            minDamage: 0,
            maxDamage: 15 
        },
        gameNotStarted: true,
        playerHealth: 100,
        monsterHealth: 100,
        disableButtons: false
    },
    methods: {
        getHealthBarColor(health) {
            if (health < 20) {
                return 'red';
            } else if (health < 60) {
                return 'yellow';
            } else {
                return 'green';
            }
        },
        resetGame() {
            this.gameNotStarted = true;
            this.playerHealth = 100;
            this.monsterHealth = 100;
            this.disableButtons = false;
            this.moves = [];
        },
        attack(monsterAttack, playerAttack=this.normalAttack) {
            let {minDamage: min, maxDamage: max} = playerAttack;
            let damage = Math.round(Math.random() * (max - min) + min);
            this.moves.unshift({volume: damage, action: 'PLAYER HITS MONSTER FOR'});
            this.monsterHealth = this.monsterHealth - damage;
            console.warn(`Damage to monster is ${damage}`);

            if (this.monsterHealth <= 0 ) {
                this.monsterHealth = 0;
                return;
            }

            setTimeout(() => {
                let {minDamage: min2, maxDamage: max2} = monsterAttack;
                damage = Math.round(Math.random() * (max2 - min2) + min2);
                this.moves.unshift({volume: damage, action: 'MONSTER HITS MONSTER FOR'});
                this.playerHealth = this.playerHealth - damage;
                if (this.playerHealth < 0) this.playerHealth = 0;

                console.warn(`Damage to player is ${damage}`);
            }, 500)
        },
        heal() {
            let {min: minRegen, max: maxRegen} = this.healthRegen;
            let {minDamage: min, maxDamage: max} = this.normalAttack;
            let health = Math.round(Math.random() * (maxRegen - minRegen) + minRegen);
            let damage = Math.round(Math.random() * (max - min) + min);
            this.moves.unshift({volume: health, action: 'PLAYER HEALS HIMSELF FOR'});
            this.moves.unshift({volume: damage, action: 'MONSTER HITS MONSTER FOR'});
            this.playerHealth = this.playerHealth + health > 100 ? 100 : this.playerHealth + health;
            this.playerHealth = this.playerHealth - damage;
        }
    },
    watch: {
        monsterHealth(value) {
            if (value == 0) {
                this.disableButtons = true;
                setTimeout(()=>{
                    let answer = confirm(`You won the monster! Start a new game?`)
                    if (answer) {
                        this.resetGame();
                    }
                },500);
            }

        },
        playerHealth(value) {
            if (value == 0) {
                this.disableButtons = true;
                setTimeout(()=>{
                    let answer = confirm(`Monster defeated you! Start a new game?`)
                    if (answer) {
                        this.resetGame();
                    }
                },500);
            }

        }
    }
})